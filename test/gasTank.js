const GasTank = artifacts.require('./contracts/GasTank.sol')
const assert = require('assert')
const assertions = require('truffle-assertions')
const expectThrow = require('./helpers/expectThrow.js')
const oneETH = web3.utils.toWei('1', 'ether')
const tenETH = web3.utils.toWei('10', 'ether')
const oneTwentyETH = web3.utils.toWei('120', 'ether')

contract('GasTank', (accounts) => {
  var contractInstance

  describe('1.0. Basic functionality', function () {
    it('should initialize the mapping correctly', async () => {
      contractInstance = await GasTank.new([accounts[0]], 1, 1, 100)
      var res = await contractInstance.whitelist.call(accounts[0])
      assert.strictEqual(res.isIn, true)
      assert.strictEqual(res.threshold.toNumber(), 1)
    })

    it('should have its owner set to accounts[0]', async () => {
      assert.strictEqual(await contractInstance.owner.call(), accounts[0])
    })

    it('should be able to receive ether', async () => {
      var currentBalance = await web3.eth.getBalance(contractInstance.address)
      assert.strictEqual(currentBalance.toString(), '0')
      await web3.eth.sendTransaction({
        from: accounts[0],
        to: contractInstance.address,
        value: oneETH
      })
      var afterSendBalance = await web3.eth.getBalance(contractInstance.address)
      assert.strictEqual(oneETH, afterSendBalance)
    })

    it('only owner should be able to add accounts to the whitelist', async () => {
      let res = await contractInstance.whitelist_add.sendTransaction(accounts[1], 10000, {
        from: accounts[0],
        to: contractInstance.address,
        value: 0
      })

      assert.strictEqual(res.receipt.status, true)
    })

    it('owner should not be able to add the same account to the whitelist twice', async () => {
      await expectThrow(contractInstance.whitelist_add.sendTransaction(accounts[1], 10000, {
        from: accounts[0],
        to: contractInstance.address,
        value: 0
      }))
    })

    // this test doesn't actually reach the contract, because apparently Solidity 0.5.x
    // is rejecting this an an invalid address
    it('owner should not be able to add null account to the whitelist ', async () => {
      const zero = '0x0000000000000000000000000000000000000000'
      await expectThrow(contractInstance.whitelist_add.sendTransaction(zero, 10000, {
        from: accounts[0],
        to: contractInstance.address,
        value: 0
      }))
    })

    it('only owner should be able to remove accounts to the whitelist', async () => {
      let res = await contractInstance.whitelist_remove.sendTransaction(accounts[1], {
        from: accounts[0],
        to: contractInstance.address,
        value: 0
      })

      assert.strictEqual(res.receipt.status, true)
    })

    it('owner should not be able to remove accounts from the whitelist twice', async () => {
      await expectThrow(contractInstance.whitelist_remove.sendTransaction(accounts[1], {
        from: accounts[0],
        to: contractInstance.address,
        value: 0
      }))
    })

    it('should be able to send gas to whitelisted accounts if threshold and block number are correct', async () => {
      contractInstance = await GasTank.new([accounts[0]], 1, 1, 100)
      await web3.eth.sendTransaction({
        from: accounts[0],
        to: contractInstance.address,
        value: oneETH
      })
      await contractInstance.whitelist_add.sendTransaction(accounts[1], web3.utils.toWei('10000', 'ether'), {
        from: accounts[0],
        to: contractInstance.address,
        value: 0
      })
      let res = await contractInstance.request_gas.sendTransaction({ from: accounts[1] })
      assert.strictEqual(res.receipt.status, true)
    })

    it('shoud not accept non whitelisted accounts to request gas', async () => {
      await assertions.reverts(contractInstance.request_gas.sendTransaction({ from: accounts[2] }))
    })
  })

  describe('2.0. Thresholds', function () {
    describe('2.1. Block thresholds', function () {
      let tank
      it('should create a tank with a 10-block threshold', async () => {
        tank = await GasTank.new([accounts[2]], oneTwentyETH, 10, oneETH, { value: tenETH })
      })
      for (let i = 0; i < 11; i++) {
        if (i === 9) {
          it('should fulfill the tenth request', async () => {
            let res = await tank.request_gas.sendTransaction({ from: accounts[2] })
            assert.strictEqual(res.receipt.status, true)
          })
        } else {
          it('should not fulfil any but the tenth request', async () => {
            await expectThrow(tank.request_gas.sendTransaction({ from: accounts[2] }))
          })
        }
      }
    })

    describe('2.2. Balance thresholds', function () {
      let tank
      it('should create a tank with a 120-ETH threshold which distributes 1 ETH at a time', async () => {
        tank = await GasTank.new([accounts[7]], oneTwentyETH, 1, web3.utils.toWei('1', 'ether'), { value: web3.utils.toWei('22', 'ether') })
      })

      it('should fulfill the first twenty-one requests for one ETH', async () => {
        for (let i = 0; i < 21; i++) {
          let res = await tank.request_gas.sendTransaction({ from: accounts[7] })
          assert.strictEqual(res.receipt.status, true)
        }
      })

      // it's the 22nd that fails because the requestor spends gas asking for gas
      it('should not fulfill the 22nd request', async () => {
        await expectThrow(tank.request_gas.sendTransaction({ from: accounts[7] }))
      })
    })
  })

  describe('3.0. Whitelist', async function () {
    let tank
    const whitelist = [accounts[4], accounts[5], accounts[6]]
    it('should create a whitelisted tank with 1000 ETH, 1 block thresholds', async () => {
      tank = await GasTank.new(whitelist, oneTwentyETH, 1, oneETH, { from: accounts[0], value: tenETH })
    })

    it('should send requested gas to each of the whitelisted accounts', async () => {
      await whitelist.map(async addr => {
        let res = await tank.request_gas.sendTransaction({ from: addr })
        assert.strictEqual(res.receipt.status, true)
      })
    })

    it('should not send gas on request to an unknown account', async () => {
      await expectThrow(tank.request_gas.sendTransaction({ from: accounts[3] }))
    })

    it('should be able to whitelist an unknown account and then send on request', async () => {
      await tank.whitelist_add.sendTransaction(accounts[3], oneTwentyETH, {
        from: accounts[0],
        to: contractInstance.address,
        value: 0
      })
      let res = await tank.request_gas.sendTransaction({ from: accounts[3] })
      assert.strictEqual(res.receipt.status, true)
    })
  })
})
