# Tanque de gas

#### Cómo distribuir el combustible para la computación en redes privadas/permisionadas basadas en Ethereum



## Motivación

Uno de los principales problemas a la hora de la creación y gestión de redes privadas/permisionadas es cómo distribuir el gas necesario para que sus participantes puedan hacer uso de la propia red. Desplegar contratos inteligentes o modificar su estado tiene un coste en gas para limitar el alcance de estas acciones.

Este gas es creado en las primeras etapas del despliegue de la red y puede seguir siendo creado durante su funcionamiento gracias a la labor que realizan los creadores/validadores de bloques. Un usuario que quiere utilizar el poder de computación de dicha red debe enviar gas a los nodos que realizan la computación, esto se hace por dos motivos:

- Limitar ejecuciones infinitas
- Incentivar a los demás nodos para que verifiquen y validen nuestras acciones, llegando así a un consenso sobre el estado de la red. Esta parte es muy importante en redes publicas, pero no tanto en redes privadas/permisionadas.

El gas marca la capacidad de ejecución de un nodo y por lo tanto es un bien bastante preciado, no tenerlo implica solo poder leer el estado de la red pero nunca modificarlo.

Terminada esta breve introducción, nos vienen diferentes preguntas a la cabeza:

- ¿ Quién es el encargado de distribuir el gas ?
- ¿ Cuánto gas deberiamos tener y cómo se distribuye a partir del primer bloque de la red ?
- ¿ Debe el gas tener un valor económico igual que en las redes públicas ?
- ¿ Cómo generamos gas ?
- ¿ Cuánto gas deberia tener cada participante de la red ?
- ¿ Cómo obtiene gas un participante ?
- ¿ Cómo se incentiva a los participantes (nodos regulares y nodos validadores) ?

En este documento intentaremos dirigir y solventar todas las preguntas que se han planteado. Consideramos que una buena resolución las diferentes cuestiones es llegar un punto en el que la relación entre gas que un participante puede disponer y el aporte en computación o memoria que hace a la red es beneficioso para todos.

Redes como Alastria no son un negocio, son futuras estructuras de estado cuyo único objetivo debe ser abrazar los beneficios del blockchain en pro de todo el país. Todo debe girar entorno a la **disminución de la deuda social**, entendida como el deber de cooperar activamente para reducir las privaciones y carencias que afectan el derecho a una vida plena y digna, en un contexto de libertad, trabajo e igualdad de oportunidades y progreso social.



## Propuesta

Algunos comentarios previos:

- La recompensa para un validador se calcula cómo: $(startGas - remainingGas) * gasPrice$
- Un `gasPrice = 0` es una mala decisión pues en un momento determinado cualquier nodo podria colapsar la red si no disponemos de mecanismos de contención.
- Dado que todo el gas esta controlado desde el propio diseño de la red, con cualquier cantidad entera `> 0` para el `gasPrice` podemos selecionar un valor aleatorio que nos condicionará las cantidades de gas, tanto en la creación  como en la y distribuición.

La idea es desplegar un contrato inteligente (banco de gas) en el mismo momento del despliegue de la red, se suele conocer como contrato inteligente precompilado. Este contrato vive en una dirección definida al inicio y conocida por todos los participantes de la red.

La función de este contrato es la de controlar todo el gas de la red desde el primer bloque. Por motivos de implementación la cantidad inicial de gas creada será de una sola cuenta, pero ésta transferirá todo su balance al contrato del banco de gas (imediatamente) que esta controlado por:

- opción 1: otro contrato especifico al uso controlado por los validadores
- opción 2: una cartera multifirma (por ejemplo Gnosis Multisig Wallet)
- opción 3: (previsión de futuro) una DAO

El banco de gas esta formado por:

- Una *whitelist* donde están todas las cuentas/dapps registradas
- Cada cuenta tiene asociados tres elementos:
  - Una tupla `limite` que tiene un limite superior e inferior que marcan el gas mínimo que la cuenta debería tener y el máximo. El máximo no es indispensable, pero nos sirve para que una cuenta no pueda pedir gas al banco y acumularlo indefinidamente
  - Un entero `bloques` que marca la cantidad de bloques que deben ser creados antes de volver a tener la capacidad de pedir gas
  - Un entero `cantidad` que define la cantidad de gas que se envia en cada petición aceptada
- Un limite `gas` que permite al banco de gas no aceptar el gas producido por los validadores (se explica a continuación). Si la transación anterior ha elevado el saldo del banco de gas por encima del limite `gas` todo el gas que se reciva en un futuro será enviado a la cuenta `0x0`.

- Una serie de funciones que permiten:
  - Añadir/Quitar de la *whitelist*
  - Cambiar los elementos que cada cuenta tiene asociades
  - Pedir gas a cualquier actor de la red
  - Actualizar el banco de gas (contrato proxy)

A parte del banco de gas haremos uso de un contrato llamado *block reward* el cual permite controlar las recompensas que reciven los validadores a la hora de la creación/validación de los bloques. Nos servirá para que los validadores no controlen el gas que generan.

Podemos diferenciar tres tipos de cuentas:

- Validadoras: las cuentas que se usan para crear los bloques (mineros)
- Dapps: cuentas que sirven a dapps
- Uso personal: cuentas de uso personal

Otra opción sería desplegar diferentes bancos de gas según el tipo de uso de la cuenta (dapp o uso personal).

Como todas las cuentas poseerán el gas que necesiten para su uso (siempre y cuando el uso esté adecuado al tipo de cuenta y variables como volumen) desaparece la necesidad/posibilidad de la creación de un mercado de compra venta del gas (capacidad de ejecución) y su valor monetario es 0.

Los elementos asociados a cada cuenta (`limite`, `bloques`, `cantidad`) se deben calcular con lo que vamos a llamar **gasTest** que es una herramienta que nos permite precalcular de una forma aproximada la cantidad de gas que necesitará una cuenta según sus necesidades.

Algunos de los posibles parámetros a considerar, según el tipo de cuenta:

- Validadora: una cantidad mínima para su funcionamiento
- Dapp:
  - según usuarios potenciales
  - coste de las funciones de los contratos
  - coste de despliegues de contratos
- Uso personal:
  - según contratos
  - diferentes niveles de uso (a debatir en mas profundidad)

Siempre se va a considerar un escenario medio (ni worst case ni ir siempre al límite).

Todas las cuentas validadoras, Dapps y uso personal muy intenso (falta definir) deben:

- Poseer un nodo completamente sincronizado con un tiempo de funcionamiento superior al 95% del día (se puede comprobar con oraculos)
- Según su uso aportar más o menos hardware a la red

Las empresas/particulares que se benefician del uso de la red són los que deben aportar los recursos en forma de hardware para el buen funcionamiento de la red. Una parte de esto, en un futuro, deberá ser aportada por el Estado pues todos sus habitantes podrán acceder a los beneficios de esta tecnologia.

Formar parte de la red no cuesta dinero. Con registrar una cuenta con tu identidad digital asociada es suficiente para acceder a la red.



![image-20190408104923975](res/Untitled_Diagram__2_.png)



Un participante puede obtener gas llamando a la función `request` del banco de gas. Vale la pena estudiar como automatizar también este proceso sin causar estragos a la red.



## Implementación

