pragma solidity ^0.5.4;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract GasTank is Ownable {
    // EVENTS
    event WhitelistAdded(address indexed added, uint256 indexed threshold);
    event WhitelistRemoved(address indexed removed);
    event GasTransferred(address indexed to, uint256 indexed amount);

    struct Status {
        bool isIn;
        uint256 threshold; // in wei
        uint256 lastGiving; // block number since last transfer
    }

    // STATE
    mapping(address => Status) public whitelist;
    uint256 private amount;
    uint256 private block_threshold;

    /**
     * @param _initial List of initial addresses to add to the whitelist
     * @param _threshold Threshold amount in Wei
     * @param _block_threshold Block number threshold
     * @param _amount Amount to send by default in Wei
     */
    constructor(address[] memory _initial, uint256 _threshold, uint256 _block_threshold, uint256 _amount) public payable {
        for(uint256 i = 0; i < _initial.length; i++) {
            whitelist[_initial[i]].isIn = true;
            whitelist[_initial[i]].threshold = _threshold;
            whitelist[_initial[i]].lastGiving = block.number;
        }
        block_threshold = _block_threshold;
        amount = _amount;
    }

    /**
     * @param _account Account to add to the whitelist
     * @param _threshold Threshold amount in Wei
     */
    function whitelist_add(address _account, uint256 _threshold) public onlyOwner {
        require(!whitelist[_account].isIn, "Account already in whitelist");
        require(_account != address(0x0), "Address can not be 0x0");
        whitelist[_account].isIn = true;
        whitelist[_account].threshold = _threshold;
        whitelist[_account].lastGiving = block.number;
        emit WhitelistAdded(_account, _threshold);
    }

    /**
     * @param _account Account to remove from the whitelist
     */
    function whitelist_remove(address _account) public onlyOwner {
        require(whitelist[_account].isIn, "Account not in whitelist");
        delete whitelist[_account];
        emit WhitelistRemoved(_account);
    }

    /**
     * @param _account Account to send gas
     */
    function send_gas(address payable _account) internal {
        whitelist[_account].lastGiving = block.number;
        _account.transfer(amount);
        emit GasTransferred(_account, amount);
    }

    // called by someone when they need gas
    function request_gas() external {
        require(whitelist[msg.sender].isIn, "Account not in whitelist");
        require(whitelist[msg.sender].lastGiving + block_threshold <= block.number, "Called outside blocks threshold");
        require(address(msg.sender).balance <= whitelist[msg.sender].threshold, "You have enough gas, threshold surpassed");
        send_gas(msg.sender);
    }

    // Enables this contract to receive ether
    function () external payable {}
}
