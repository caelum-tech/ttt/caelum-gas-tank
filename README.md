# Caelum Gas Tank

|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/caelum-gas-tank/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/caelum-gas-tank/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/caelum-gas-tank/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/caelum-gas-tank/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/caelum-gas-tank/commits/master)|

## Overview


**Caelum Gas Tank** (CGT) is a smart contract that allows users to request gas.
Users must be in the smart contract whitelist and comply with two requirements:
- The number of blocks sealed since the last gas request must be higher than block threshold
- The amount of gas that an account holds must be less than or equal to the gas threshold

## Work flow

1. An administrator deploys the GasTank using as params:
   - a list of accounts to add into whitelist
   - a wei threshold amount
   - a block threshold
   - the amount of gas to send on every request.
2. Gas tank needs ether, anyone can send ether into the gas tank
3. The owner (administrator) of the contract can add or remove whitelisted accounts
4. If gas tank has ether any user in the whitelist can request gas calling to the function `request_gas`
5. If the user complies with the thresholds the amount defined is sent to the user

## Limitations, or not.

Block threshold and the amount to send on gas request is the same for every whitelisted user.

## Usage
Deploy on your own network or pass tests

```npm i && npm test```
